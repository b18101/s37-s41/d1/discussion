//[SECTION] DEPENDENCIES
const jwt = require('jsonwebtoken');
const secret = "CourseBookingAPI";

/*
	Notes:

	JWT is a way to securely pass information from one part of a server to the frontend or other parts of our application. This will allow us to authorize our users to access or disallow acces to certain parts of our app.

	JWT is like a gift wrapping service that is able to encode our user details which can only be unwrapped by jwt's own methods and if the secret provided is intact.

	IF the JWT seemed tampered with, we will reject the users attempt to access a feature

*/

module.exports.createAccessToken = (user) => {
	console.log(user);

	// data object is created to contain some details of user
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	console.log(data);

	return jwt.sign(data, secret, {})
}

// NOTES:
/*
		Notes:

		>> You can only get a unique jwt with our secret if you log in to our app with the correct email and password

		>> As a user, you can only get your own details from your own token from logging in

		>> JWT is not meant to store sensitive data. For now, for ease of use and for our MVP (minimum viable product), we add the email and isAdmin details of the logged in user. However, in the future, you can limit this to only id and for every route and feature, you can simply look up for the user in the database to get his details

		>> We will verify the legitimacy of a JWT every time a user access a restricted feature. Each JWT contains a secret only our server knows. IF the jwt has been changed in any way, we will reject the user and his tampered token. IF the jwt does not contain a sceret OR the secret is different, we will reject his access and token

	*/

// goal: to check if the token exist or it is not tampered

module.exports.verify = (req, res, next) => {

	// req.headers.authorization- contains jwt/ token
	let token = req.headers.authorization;

	if(typeof token === "undefined"){
		// typeof result is a string

		return res.send({auth: "Failed. No Token"})
	
	} else {
		console.log(token);

		/*
			slice method is used for arrays and strings
			
			Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyOTZmZWExMmU0NTc0NWI4NjE3ODAyMSIsImVtYWlsIjoiY2FsbE1lTGlzYUBnbWFpbC5jb20iLCJpc0FkbWluIjpmYWxzZSwiaWF0IjoxNjU0MTMxODQyfQ.RVmGn5gzvC1LChXKomeXSsiW0rFqZNDQSmEmvN2_C4o

		*/
		token = token.slice(7, token.length)
		console.log(token)

		/*
			expected output:

			eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyOTZmZWExMmU0NTc0NWI4NjE3ODAyMSIsImVtYWlsIjoiY2FsbE1lTGlzYUBnbWFpbC5jb20iLCJpc0FkbWluIjpmYWxzZSwiaWF0IjoxNjU0MTMxODQyfQ.RVmGn5gzvC1LChXKomeXSsiW0rFqZNDQSmEmvN2_C4o
		*/

		jwt.verify(token, secret, (err, decodedToken) => {

			if(err){
				return res.send({
					auth: "Failed",
					message: err.message
				})
			
			} else {
				console.log(decodedToken);

				req.user = decodedToken

				// will let us proceed to the next middleware or controller
				next();
			}
		})
	}
};

// verifying an admin

module.exports.verifyAdmin = (req, res, next) => {

	if(req.user.isAdmin){

		next();
	
	} else {

		return res.send({
			auth: "Failed",
			message: "Action Forbidden"
		})
	}
};
