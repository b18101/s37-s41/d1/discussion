// [SECTION] DEPENDENCIES 
const Course = require("../models/Course");

// [SECTION] ADD COURSE
module.exports.addCourse = (req,res) => {

	console.log(req.body);

	let newCourse = new Course({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price

	})

	newCourse.save()
	.then(course => res.send(course))
	.catch(error => res.send(error))
}

// [SECTION] GET ALL COURSES
module.exports.getAllCourses = (req,res) => {

	//find all documents in the Course collection
	//We will use the find() method of our Course model
	//Because the Course model is connected to our courses collection

	Course.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));

}

// [SECTION] GET SINGLE COURSE
module.exports.getSingleCourse = (req,res) => {

	//console.log(req.params.id)//Check the if you can receive the data coming from the url.

	//let id = req.params.id

	Course.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err))

}
// [SECTION] UPDATING A COURSE

module.exports.updateCourse = (req, res) => {

	let updates = {

		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	Course.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(err => res.send(err))
}

// [SECTION] ARCHIVE A COURSE
module.exports.archive = (req,res) => {

	//console.log(req.params.id);//course's id

	//updates object will contain the field/fields to update and its new value.
	let updates = {
		isActive: false
	}

	Course.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(err => res.send(err));

}

// [SECTION] ACTIVATE A COURSE
module.exports.activate = (req,res) => {

	
	let updates = {
		isActive: true
	}

	Course.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(err => res.send(err));

}

// [SECTION] GET ALL ACTIVE COURSES
module.exports.getActiveCourses = (req,res) => {

	Course.find({isActive: true})
	.then(result => res.send(result))
	.catch(err => res.send(err));

}

// [SECTION] FIND COURSES BY NAME
module.exports.findCoursesByName = (req, res) => {

	console.log(req.body) // contain the name of the course you are looking for

	Course.find({name: {$regex: req.body.name, $options: '$i'}})
	.then(result => {

		if(result.length === 0){
			return res.send('No courses found')
		
		} else {

			return res.send(result)
		}
	})
}
