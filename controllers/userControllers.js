// [SECTION] DEPENDENCIES 
const User = require('../models/User');
const Course = require('../models/Course');
const bcrypt = require('bcryptjs');
const auth = require('../auth')

// [SECTION] REGISTER USER
module.exports.registerUser = (req, res) => {

	console.log(req.body);

	/*

		bcrypt- add a layer of security to your user's password

		What bcrypt does is hash our password into a randomized character version of the original string

		bcrypt.hashSync(<stringToBeHashed>, <saltRounds>)

		salt rounds- number of times the characters in the hash randomized

		let password = req.body.password
	*/
	
	const hashedPW = bcrypt.hashSync(req.body.password, 10);

	// Create a new user document out of our user model

	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		mobileNo: req.body.mobileNo,
		password: hashedPW
	});

	newUser.save()
	.then(user => res.send(user))
	.catch(err => res.send(err));
};

// [SECTION] RETRIEVAL OF ALL USERS

module.exports.getAllUsers = (req, res) => {

	User.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

// [SECTION] LOGIN USER

module.exports.loginUser = (req, res) => {

	console.log(req.body);

	/*
		1. find the user by the email
		2. If we found user, we will check the password
		3. If we don't find the user, then we will send a message to the client
		4. If upon checking the found user's password is the same as our input password, we will generate the 'token / key' to access our app. If not, we will turn them away by sending a message to the client

	*/

	User.findOne({email: req.body.email})
	.then(foundUser => {

		if(foundUser === null){
			return res.send("No user found in the database");

		} else {

			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)
			console.log(isPasswordCorrect);

			/*
				compareSync()
				will return a boolean value
				so if it matches, this will return true
				if not, this will return false

			*/

			if(isPasswordCorrect){

				return res.send({accessToken: auth.createAccessToken(foundUser)})

			} else {

				return res.send("Incorrect password, please try again")
			}
		}
	})
	.catch(err => res.send(err));
};

// [SECTION] GETTING SINGLE USER DETAILS

module.exports.getUserDetails = (req, res) => {

	console.log(req.user);
	/*
	expected output: decoded token
		{
		  id: '6296fea12e45745b86178021',
		  email: 'callMeLisa@gmail.com',
		  isAdmin: false,
		  iat: 1654136799
		}

	*/

	// find the logged in user's document from our db and send it to the client by its id

	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

// [SECTION] CHECK IF EMAIL EXISTS

module.exports.checkEmailExists = (req,res) => {

	//console.log(req.body.email);//check if you can receive the email from our client's request body.
	
	//You can use find({email: req.body.email}), however, find() returns multiple documents in an ARRAY.

	User.findOne({email: req.body.email})
	.then(result => {

		//console.log(result)

		if(result === null){
			return res.send("Email is available.");
		} else {
			return res.send("Email is already registered.")
		}

	})
	.catch(err => res.send(err));
}

// [SECTION] UPDATING USER DETAILS

module.exports.updateUserDetails = (req, res) => {
	
	console.log(req.body); //input for new values
	console.log(req.user.id); // check the logged in user's id

	let updates = {
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo
	}

	User.findByIdAndUpdate(req.user.id, updates, {new: true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err));	
};

// [SECTION] UPDATE AN ADMIN

module.exports.updateAdmin = (req, res) => {

	console.log(req.user.id); // id of the logged in user

	console.log(req.params.id); // id of the user we want to update

	let updates = {

		isAdmin: true
	}

	User.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err));
};

// [SECTION] ENROLLMENT

// async- makes the function asynchronous

module.exports.enroll = async (req, res) => {
	
	/*
		Steps:
		1. Look for the user by its id
			-- push the details of the course we're trying to enroll in. We'll push to a new subdocument in our user

		2. Look for the course by its id
			-- push the details of the user/ enrollee who's trying to enroll. We'll push to a new enrollees subdocument in our course

		3. When both saving of documents are successful, we send a message to the client
		
	*/	

	console.log(req.user.id) //the user's id from the decoded token after verify
	console.log(req.body.courseId) // the course from our request body
	console.log(req);


	// Checking if a user is an admin, if he is an admin then he should not be able to enroll
	if(req.user.isAdmin){
		return res.send("Action Forbidden")
	}

	/*
		Find the user:

		async- a keyword that allows us to make our function asynchronous. Which means, that instead of JS regular behavior of running each code line by line, it will allow us to wait for the result of the function.

		await- a keyword that allows us to wait for the function to finish before proceeding

	*/

	let isUserUpdated = await User.findById(req.user.id).then(user => {

		console.log(user);
		/*
			{
				id: 48g4sa54ds2d1s,
				firstName: <userValue>
				lastName: <userValue>
				email: <userValue>
				password: <userValue>
				mobileNo: <userValue>
				isAdmin: <userValue>
				enrollments: [{<userValue>}]
			}
		*/

		// add the courseId in an object and push that object into user's enrollment array
		let newEnrollment = {
			courseId: req.body.courseId
		}

		user.enrollments.push(newEnrollment)

		return user.save()
		.then(user => true)
		.catch(err => err.message)
	})

		// if isUserUpdated does not contain the boolean value of true, we will stop our process and return res.send() to our client with our message
		if(isUserUpdated !== true){
			return res.send({message: isUserUpdated})
		}


		let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {

			console.log(course)

			/*
				{
					id: 45f4as65f4s54f5sa4f8c
					name- string,
					description- string,
					price- number,
					isActive- boolean,
							default: true
					createdOn: date
					enrollees: [
						{
							userId: string,
							status: string,
							dateEnrolled: date
						}
					]
				}

			*/

			// create an object which will contain the user id of the enrollee of a course
			let enrollee = {
				userId: req.user.id
			}

			course.enrollees.push(enrollee)

			return course.save()
			.then(course => true)
			.catch(err => err.message)
		})

		if(isCourseUpdated !== true){

			return res.send({message: isCourseUpdated})
		}

		if(isUserUpdated && isCourseUpdated){
			return res.send({message: 'User enrolled successfully'})
		}
};

// [SECTION] GET ENROLLMENTS

module.exports.getEnrollments = (req, res) => {

	User.findById(req.user.id)
	.then(result => res.send(result.enrollments))
	.catch(err => res.send(err))
};
