// [SECTION] DEPENDENCIES
const express = require('express');
const router = express.Router();

// [SECTION] IMPORTED MODULES
const userControllers = require('../controllers/userControllers');
const auth = require('../auth');

//object destructuring from auth module
const { verify, verifyAdmin } =  auth;

// [SECTION] ROUTES

// register user
router.post('/', userControllers.registerUser);

// get all user
/*
	Mini-Activity: 3 mins
	>> endpoint: '/'
	Create a route for getting all users. 
	Use the appropriate HTTP method

	>> Send your outputs in Hangouts

*/
router.get('/', userControllers.getAllUsers);

//login user
router.post('/login', userControllers.loginUser);

// get user details
router.get('/getUserDetails', verify, userControllers.getUserDetails)

// Check if Email Exists
router.post('/checkEmailExists',userControllers.checkEmailExists);

// Updating user details

router.put('/updateUserDetails', verify, userControllers.updateUserDetails);

/*
	Mini-Activity : 10 mins
		>> Create a user route which is able to capture the id from its url using route params
			-- This route will only update a regular user to an admin
			-- Only an admin can access this route
			-- Test the route in postman
			-- Send your outputs in Hangouts

*/
router.put('/updateAdmin/:id', verify, verifyAdmin, userControllers.updateAdmin);

router.post('/enroll', verify, userControllers.enroll);

router.get('/getEnrollments', verify, userControllers.getEnrollments);

module.exports = router;
