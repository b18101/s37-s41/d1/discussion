// [SECTION] DEPENDENCIES 
const express = require("express");
const router = express.Router();

// [SECTION] IMPORTED MODULES
const courseControllers = require("../controllers/courseControllers");
const auth = require('../auth');

const {verify, verifyAdmin} = auth;


//create/add course
router.post('/', verify, verifyAdmin, courseControllers.addCourse);

//get all courses
router.get('/', courseControllers.getAllCourses);

//get single course
router.get('/getSingleCourse/:id',courseControllers.getSingleCourse);

// update a course
router.put('/:id', verify, verifyAdmin, courseControllers.updateCourse);
/*
	>> Try the route in Postman
	>> Send your updated course in Hangouts
*/

//archive
router.put('/archive/:id',verify, verifyAdmin,courseControllers.archive);

//activate
router.put('/activate/:id',verify, verifyAdmin,courseControllers.activate);

//get active courses
router.get('/getActiveCourses',courseControllers.getActiveCourses);

// find courses by name
router.post('/findCoursesByName', courseControllers.findCoursesByName);

module.exports = router;
